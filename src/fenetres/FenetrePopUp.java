package fenetres;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import meteores.Thread_meteores;

public class FenetrePopUp extends JFrame implements MouseListener{


	JOptionPane jop1;
	private JButton bouton = new JButton("Oui");
	private JButton bouton2 = new JButton("Non");

	public FenetrePopUp() {

		jop1 = new JOptionPane();
		ImageIcon img = new ImageIcon(FenetrePopUp.class.getResource("/img/GameOver.png"));
		jop1.showConfirmDialog(null, "Vous avez perdu! Souhaitez-vous recommencer ?", "Game Over", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, img);

		bouton.addMouseListener(this);
		bouton2.addMouseListener(this);

		this.setVisible(true);		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mousePressed(MouseEvent e) {
	
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
	}


}