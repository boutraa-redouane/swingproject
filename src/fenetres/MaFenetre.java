package fenetres;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import avion.Avion;
import ecouteurs.KeyEcouteur;
import main.Tools;

public class MaFenetre extends JFrame {
	private static final long serialVersionUID = 1L;
	public static Avion monAvion;
	public static MaFenetre fen;

	public MaFenetre() {
		MaFenetre.fen = this;
		this.setSize(500,600);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		monAvion = new Avion(this);
		this.addKeyListener(new KeyEcouteur(monAvion));

		this.setLayout(new BorderLayout());

		BandeInfos bandeInfos = new BandeInfos();
		this.getContentPane().add(bandeInfos,BorderLayout.NORTH);
		
		ZoneJeux zoneJeux = new ZoneJeux(monAvion);
		this.getContentPane().add(zoneJeux,BorderLayout.CENTER);
		
		Perdu  aPerdu = new Perdu();
		this.getContentPane().add(aPerdu,BorderLayout.SOUTH);

		Rafraichisseur monRafraichisseur = new Rafraichisseur(this);
		monRafraichisseur.start();

		
		this.setVisible(true);
		
	}


}



