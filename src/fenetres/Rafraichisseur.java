package fenetres;

import meteores.Meteor;
import meteores.MeteorsFactory;

public class Rafraichisseur extends Thread {

	private MaFenetre mafenetre;

	public Rafraichisseur(MaFenetre f) {
		this.mafenetre = f;
	}

	@Override
	public void run() {
		while (true) {
			this.mafenetre.repaint();
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
