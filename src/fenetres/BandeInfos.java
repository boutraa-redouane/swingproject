package fenetres;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import avion.Avion;

public class BandeInfos extends JPanel {
	public static Avion monAvion;
	public static MaFenetre fen;
	public static JLabel LabVies;
	public static JLabel LabScore;

	public BandeInfos() {
		this.setSize(100, 50);
		this.setBackground(Color.CYAN);
		
		this.setLayout(new GridLayout(1,3));
		
		LabScore = new JLabel ("Score : "+MaFenetre.monAvion.score);
		this.add(LabScore);
		
		JLabel LabNom = new JLabel ("Nom : "+Avion.nom);
		this.add(LabNom);
		
		LabVies = new JLabel("Nbre Vies : "+MaFenetre.monAvion.vie);
		this.add(LabVies);
	
	}
	
}
