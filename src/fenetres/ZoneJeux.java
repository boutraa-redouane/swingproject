package fenetres;

import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;

import javax.swing.JPanel;

import avion.Avion;
import main.Tools;
import meteores.Meteor;
import meteores.MeteorsFactory;
import meteores.Thread_meteores;

public class ZoneJeux extends JPanel {

	private Avion monAvion;
	Image back ;
	public static boolean again = true;

	public ZoneJeux(Avion v) {
		this.monAvion = v;
		this.back = Tools.initialisationImage("/img/back.jpg");
		new Thread_meteores().start();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(back, 0, 0, getWidth(), getHeight(), null);
		
		for (Meteor ele : MeteorsFactory.liste) {
			ele.dessiner(g);
		}		
		this.monAvion.dessiner(g);	
		
		
	}


}
