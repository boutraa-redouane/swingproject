package main;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import javax.imageio.ImageIO;

import meteores.Meteor;


public class Tools {

	public static String SaisieNom () {

		Scanner sc = new Scanner(System.in);
		String s=null;
		boolean fin = true;

		while (fin) {
			System.out.print("Veuillez saisir votre nom : ");
			s=sc.next();
			sc.nextLine();

			if (s.contains(";") || s.length()<3 || s.length()>6) {
				System.out.println("Saisie incorrecte (entre 3 et 6 caractères, sans point-virgule)");
			}
			else {
				fin=false; }
		}
		sc.close();
		return s;
	}

	public static void saveScore (String nom,int score) throws IOException {

		Path path = Paths.get(System.getProperty("java.io.tmpdir")+File.separator+"avion-cda.txt");

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date ();
		String str=sdf.format(date);

		String text =nom+";"+score+";"+str+"\n";

		Files.write(path,text.getBytes(),StandardOpenOption.CREATE,StandardOpenOption.WRITE,
				StandardOpenOption.APPEND);

	}

	public static void readScore(){

		try {
//			String str = "temp\\save.txt";
			Path src = Paths.get(System.getProperty("java.io.tmpdir")+File.separator+"avion-cda.txt");
			List<String> lignes = Files.readAllLines(src);

			for (int i=0;i<lignes.size()-1;i++) {
				String s=lignes.get(i);
				String [] tablo = s.split(";");
				System.out.println(tablo[0]+"  "+tablo[1]+"  "+tablo[2]);
			}

			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static BufferedImage initialisationImage(String s) {
		try {
			return ImageIO.read(Meteor.class.getResource(s));
		} catch(Exception e) {
			throw new RuntimeException(e); 
		}
	}

}





