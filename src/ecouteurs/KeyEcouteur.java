package ecouteurs;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import avion.Avion;

public class KeyEcouteur extends KeyAdapter{
	Avion monAvion;

	@Override
	public void keyPressed(KeyEvent e) {
		int a = e.getKeyCode();
		if(a == 37) {
			monAvion.setPosX("GAUCHE");
			Avion.command = 3;
		}
		if(a == 38) {
			monAvion.setPosY("HAUT");
			Avion.command = 1;
		}
		if(a == 39) {
			monAvion.setPosX("DROITE");
			Avion.command = 2;
		}
		if(a == 40) {
			monAvion.setPosY("BAS");
			Avion.command = 1;
		}
	}

	public KeyEcouteur(Avion monAvion) {
		this.monAvion = monAvion;
	}
}
