package meteores;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import fenetres.MaFenetre;
import main.Tools;

public class MeteorIceberg extends Meteor{
	public static BufferedImage img = Tools.initialisationImage("/img/MeteoreIceberg.png");
	
	private static final int VITESSE = 2;
	private static final int POINTSENMOINS = 4;
	private static final int SCOREENPLUS = 5;
	private static final int TAILLE = 45; 
	
	public MeteorIceberg() {
		
		this.vitesse = VITESSE;
		this.pointsEnMoins = POINTSENMOINS;
		this.scoreEnPlus = SCOREENPLUS;
		this.taille = TAILLE;
		
		posX = r.nextInt (MaFenetre.fen.getWidth());

	}

	public int getPosX() {
		return posX;
	}

	public int getPosY() {
		return posY;
	}
	
	public void move(){
		this.posY = this.posY + 3*VITESSE;
	}

	public void dessiner(Graphics g) {
		g.drawImage(this.img,this.getPosX(),this.getPosY(),TAILLE/2,TAILLE,this);
	}
}