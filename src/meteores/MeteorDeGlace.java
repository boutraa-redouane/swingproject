package meteores;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import fenetres.MaFenetre;
import main.Tools;

public class MeteorDeGlace extends Meteor {
	
	public static BufferedImage img = Tools.initialisationImage("/img/MeteoreDeGlace.png");
	
	private boolean bool = true;

	private static final int VITESSE = 2;
	private static final int POINTSENMOINS = 2;
	private static final int SCOREENPLUS = 3;
	private static final int TAILLE = 30; 
	
	public MeteorDeGlace() {
		
		this.vitesse = VITESSE;
		this.pointsEnMoins = POINTSENMOINS;
		this.scoreEnPlus = SCOREENPLUS;
		this.taille = TAILLE;
		
		posX = r.nextInt (MaFenetre.fen.getWidth());

	}

	public int getPosX() {
		return posX;
	}

	public int getPosY() {
		return posY;
	}
	
	public void move(){
		this.posY = this.posY + 3*VITESSE;
	}

	public void dessiner(Graphics g) {
		g.drawImage(img,this.getPosX(),this.getPosY(),TAILLE/2,TAILLE,this);
	}
}
