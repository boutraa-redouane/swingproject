package meteores;

import java.io.IOException;
import java.util.Random;

import javax.swing.JLabel;

import avion.Avion;
import fenetres.BandeInfos;
import fenetres.FenetrePopUp;
import fenetres.MaFenetre;
import fenetres.Perdu;
import fenetres.Rafraichisseur;
import fenetres.ZoneJeux;
import main.Tools;

public class Thread_meteores extends Thread{
	Random r = new Random();


	@Override
	public void run() {
		while(ZoneJeux.again) {
			if(MeteorsFactory.liste.size() < 3) {
				Meteor a = MeteorsFactory.create(r.nextInt(5));
			}
			try {
				Thread.sleep(150);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			for (int i=0; i< MeteorsFactory.liste.size();i++) {
				Meteor ele = MeteorsFactory.liste.get(i);
				ele.move();
				if(ele.getPosY() > MaFenetre.fen.getHeight()) {
					MaFenetre.monAvion.score = Avion.score + MeteorsFactory.liste.get(i).getScoreEnPlus();
					MeteorsFactory.liste.remove(i);
					BandeInfos.LabScore.setText("Score : "+MaFenetre.monAvion.score);
				}

				if (MaFenetre.monAvion.score>999) {
					Avion.setScore(0);
				}


				if((ele.getPosX() + ele.getWidth() - 3 > MaFenetre.monAvion.getPosX() && 
						ele.getPosX()<MaFenetre.monAvion.getPosX() + MaFenetre.monAvion.f.getWidth()/10 - 3)
						&&
						(ele.getPosY() + ele.getHeight() - 3 > MaFenetre.monAvion.getPosY() && 
								ele.getPosY()<MaFenetre.monAvion.getPosY() + MaFenetre.monAvion.f.getHeight()/10 - 3) || (ele.getPosX()+3 < MaFenetre.monAvion.getPosX() && 
										ele.getPosX()<MaFenetre.monAvion.getPosX() + MaFenetre.monAvion.f.getWidth()/12 - 3)
						&&
						(ele.getPosY() + 3 > MaFenetre.monAvion.getPosY() && 
								ele.getPosY()<MaFenetre.monAvion.getPosY() + MaFenetre.monAvion.f.getHeight()/12 - 3)){

					MaFenetre.monAvion.vie = MaFenetre.monAvion.vie - ele.pointsEnMoins;
					BandeInfos.LabVies.setText("Nbre Vies : "+MaFenetre.monAvion.vie);
					//					System.out.println(MaFenetre.monAvion.vie);

					if(MaFenetre.monAvion.vie <= 0) {
						Perdu.perdu.setText("Vous avez perdu");
						ZoneJeux.again = false;
						
						try {
							Tools.saveScore(Avion.nom, Avion.getScore());
							
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						new FenetrePopUp();
					}
				}

			}

		}

	}

}






