package meteores;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import fenetres.MaFenetre;
import main.Tools;

public class MeteorZigZag extends Meteor {
	public static BufferedImage img= Tools.initialisationImage("/img/MeteoreZigZag.png");

	static int x;
	private static final int PAS =15;

	private static final int VITESSE = 1;
	private static final int POINTSENMOINS = 2;
	private static final int SCOREENPLUS = 5;
	private static final int TAILLE = 60; 

	public MeteorZigZag() {

		this.vitesse = VITESSE;
		this.pointsEnMoins = POINTSENMOINS;
		this.scoreEnPlus = SCOREENPLUS;
		this.taille = TAILLE;

		// Position X al�atoire entre 1 pas et la largeur - 1 pas 
		x=r.nextInt ((MaFenetre.fen.getWidth()-2*PAS)+PAS);
		posX = x-PAS;

	}

	public int getPosX() {
		return posX;
	}

	public int getPosY() {
		return posY;
	}

	public void move(){
		this.posX = (2*x)-posX;
		this.posY = this.posY + 3*VITESSE;
	}

	public void dessiner(Graphics g) {
		g.drawImage(img,this.getPosX(),this.getPosY(),TAILLE/2,TAILLE,null);
	}
}
