package meteores;

import java.util.ArrayList;
import java.util.List;

public class MeteorsFactory {
	private int type; 

	public static List <Meteor> liste = new ArrayList<>();

	public MeteorsFactory() {

	}

	public static Meteor create(int m) {
		Meteor meteor = null;
		
		if(m == 0) {
			meteor = new MeteorSimple();
		}else if(m == 1) {
			meteor = new MeteorDeFeu();
		}else if(m == 2) {
			meteor = new MeteorDeGlace();
		}else if(m == 3) {
			meteor = new MeteorZigZag();
		}else if(m == 4) {
			meteor = new MeteorIceberg();
		}

		liste.add(meteor);
		return meteor;
	}
} 

