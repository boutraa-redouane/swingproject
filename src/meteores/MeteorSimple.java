package meteores;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Random;

import javax.imageio.ImageIO;

import fenetres.MaFenetre;
import main.Tools;

public class MeteorSimple extends Meteor{
	public static BufferedImage img = Tools.initialisationImage("/img/MeteoreSimple.png");;
	
	private static final int VITESSE = 2;
	private static final int POINTSENMOINS = 1;
	private static final int SCOREENPLUS = 2;
	private static final int TAILLE = 25; 
	
	public MeteorSimple() {
		
		this.vitesse = VITESSE;
		this.pointsEnMoins = POINTSENMOINS;
		this.scoreEnPlus = SCOREENPLUS;
		this.taille = TAILLE;
		
		posX = r.nextInt (MaFenetre.fen.getWidth());

	}

	public int getPosX() {
		return posX;
	}

	public int getPosY() {
		return posY;
	}

	
	public void move(){
		this.posY = this.posY + 3*VITESSE;
	}

	public void dessiner(Graphics g) {
		g.drawImage(img,this.getPosX(),this.getPosY(),TAILLE/2,TAILLE,null);
	}


} 





