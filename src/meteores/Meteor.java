package meteores;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import fenetres.MaFenetre;

public abstract class Meteor  extends JPanel {

	protected int vitesse;
	protected int pointsEnMoins;
	protected int scoreEnPlus;
	protected int taille;
	protected int positionListe;

	public int posX;
	public int posY = 0;

	static Random r = new Random();


	public abstract int getPosX();

	public int getScoreEnPlus() {
		return scoreEnPlus;
	}

	public int getPointsEnMoins() {
		return pointsEnMoins;
	}

	public abstract int getPosY();

	public abstract void move();
	
	public abstract void dessiner(Graphics g);

}