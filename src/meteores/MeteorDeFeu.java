package meteores;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import fenetres.MaFenetre;
import main.Tools;

public class MeteorDeFeu extends Meteor{
	public static BufferedImage img = Tools.initialisationImage("/img/MeteoreDeFeu.png");

	private static final int VITESSE = 1;
	private static final int POINTSENMOINS = 2;
	private static final int SCOREENPLUS = 1;
	private static final int TAILLE = 45; 

	public MeteorDeFeu() {

		this.vitesse = VITESSE;
		this.pointsEnMoins = POINTSENMOINS;
		this.scoreEnPlus = SCOREENPLUS;
		this.taille = TAILLE;
		int a = MaFenetre.fen.getWidth() - img.getWidth(); 
		posX = r.nextInt (a);
	}

	public int getPosX() {
		return posX;
	}

	public int getPosY() {
		return posY;
	}

	public void move(){
		this.posY = this.posY + 3*VITESSE;
	}

	public void dessiner(Graphics g) {
		g.drawImage(img,this.getPosX(),this.getPosY(),TAILLE/2,TAILLE,null);
	}




}
