package avion;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JLabel;

import fenetres.MaFenetre;
import main.Tools;

public class Avion {
	private int posX;
	private int posY;
	public MaFenetre f;
	public static int vie = 50;
	public static int score = 0;


	public static String nom;

	public static  Image avionImg;
	protected Image avionD;
	protected Image avionG;
	protected Image Bang;
	public static int command = 1;

	public static int pointsVie = 5;

	public Avion(MaFenetre f) {
		this.f = f;
		this.posX = f.getWidth()/2;
		this.posY = f.getHeight() - f.getHeight()*2/10;
		avionImg = Tools.initialisationImage("/img/Avion.png");//ImageIO.read(new File("img\\Avion.jpg"));
		avionD = Tools.initialisationImage("/img/AvionD.gif");//ImageIO.read(new File("img\\AvionD.gif"));
		avionG = Tools.initialisationImage("/img/AvionG.gif");//ImageIO.read(new File("img\\AvionG.gif"));
		Bang = Tools.initialisationImage("/img/Bang.png");
	}

	public int getPosX() {
		return posX;
	}
	public void setPosX(String param) {
		if(param.equals("GAUCHE")) {
			if(this.posX > 2) {
				this.posX = this.posX - 3;
			}else if(this.posX > 1) {
				this.posX = this.posX - 2;
			}else if(this.posX > 0) {
				this.posX = this.posX - 1;
			}
		}else if(param.equals("DROITE")) {
			if(this.posX < f.getWidth() -3 - f.getWidth()/10-15) {
				this.posX = this.posX + 3;
			}else if(this.posX < f.getWidth() -2 - f.getWidth()/10-15) {
				this.posX = this.posX + 2;
			}else if(this.posX < f.getWidth() - 1 - f.getWidth()/10-15) {
				this.posX = this.posX + 1;
			}
		}
	}
	public int getPosY() {
		return posY;
	}
	public void setPosY(String param) {
		if(param.equals("HAUT")) {
			if(this.posY > 2) {
				this.posY = this.posY - 3;
			}else if(this.posY > 1) {
				this.posY = this.posY - 2;
			}else if(this.posY > 0) {
				this.posY = this.posY - 1;
			}
		}else if(param.equals("BAS")) {
			if(this.posY < (f.getHeight() - 2 - f.getHeight()/10)-40) {
				this.posY = this.posY  + 3;
			}else if(this.posY < (f.getHeight() - 1 - f.getHeight()/10)-40) {
				this.posY = this.posY + 2;
			}else if(this.posY < (f.getHeight() - f.getHeight()/10)-40) {
				this.posY = this.posY + 1;
			}
		}
	}

	public void dessiner(Graphics g) {
		if(command == 1) {
			g.drawImage(avionImg,this.getPosX(),this.getPosY(),this.f.getWidth()/12,this.f.getHeight()/12,null);	
		}else if(command ==2) {
			g.drawImage(avionD,this.getPosX(),this.getPosY(),this.f.getWidth()/12,this.f.getHeight()/12,null);	
			command = 1;
		}else if(command ==3) {
			g.drawImage(avionG,this.getPosX(),this.getPosY(),this.f.getWidth()/12,this.f.getHeight()/12,null);
			command = 1;
		}
		if (vie<= 0) {
			g.drawImage(Bang,this.getPosX(),this.getPosY(),this.f.getWidth()/8,this.f.getHeight()/8,null);
		}
	}


	public static void setNom(String nom) {
		Avion.nom = nom;
	}

	public static int getScore() {
		return score;
	}

	public static void setScore(int score) {
		Avion.score = score;
	}

	public MaFenetre getF() {
		return f;
	}
}
